<?php
class Punto extends CI_Model
{
    function __construct()
    {
        parent::__construct(); //invocar clase padre
        $this->load->database();
    }
    public function insertar($datos)
    {
        return $this->db->insert("localizacion", $datos);
    }
    //funcion cosultar los datos de la tabla
    function obtenerTodos()
    {
        $this->db->order_by("id_lt", "asc");
        $localizacion = $this->db->get("localizacion");
        if ($localizacion->num_rows() > 0) { //Cuando existen clientes
            return $localizacion->result();
        } else {
            return false; //Cuando no existen clientes
        }
    }
    //Funcion para eliminar un ubicacion se recibe el id
    public function eliminarPorId($id){
        $this->db->where("id_lt",$id);
        return $this->db->delete("localizacion");//eliminar informacion de la tabla
    }
}
