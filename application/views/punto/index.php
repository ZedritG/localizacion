<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Localizacion</title>
  <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <!-- Optional theme -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <!--Api google -->
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9JGGGakvZ_PZx-L3m-TlQfEbaxuEhATs&libraries=places&callback=initMap"></script>

</head>

<body>
  <div class="row">
    <div class="col-md-6 text-center">
      <h3 class="text-center"><b>REGISTRO DE PROPIEDAD</b></h3>
      <div class="container-fluid">
        <form class="form-horizontal" action="<?php echo site_url('puntos/guardar'); ?>" method="post">
          <div class="row">
            <div class="col-md-12">
              <b>CEDULA</b>
              <input type="text" required name="cedula_lt" class="form-control" >
            </div>
            <div class="col-md-12">
              <b>APELLIDOS</b>
              <input type="text" required name="apellido_lt" class="form-control" value="">
            </div>
            <div class="col-md-12">
              <b>NOMBRES</b>
              <input type="text" required name="nombre_lt" class="form-control" value="">
            </div>
            <div class="col-md-6">
              <b>CELULAR</b>
              <input type="text" required name="celular_lt" class="form-control" value="">
            </div>
            <div class="col-md-6">
              <b>E-MAIL</b>
              <input type="text" required name="email_lt" class="form-control" value="">
            </div>
          </div>
          <br>
          <b>MAPA 1</b>
          <div class="row">
            <div class="col-md-6">
              <b>LATITUD</b>
              <input type="text" required name="marcador1_lt" class="form-control" value="" id="latitud_lt1" readonly>
            </div>
            <div class="col-md-6">
              <b>LONGITUD</b>
              <input type="text" required name="marcador1_ln" class="form-control" value="" id="longitud_lt1" readonly>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-12">
              <div class="" id="mapa1" style="border: 1px solid black; height: 250px; width: 100%">
              </div>
            </div>
          </div>
          <br>
          <b>MAPA 2</b>
          <div class="row">
            <div class="col-md-6">
              <b>LATITUD</b>
              <input type="text" required name="marcador2_lt" class="form-control" value="" id="latitud_lt2" readonly>
            </div>
            <div class="col-md-6">
              <b>LONGITUD</b>
              <input type="text" required name="marcador2_ln" class="form-control" value="" id="longitud_lt2" readonly>
            </div>
          </div>
          <div class="row">
          <br>
            <div class="col-md-12">
              <div class="" id="mapa2" style="border: 1px solid black; height: 250px; width: 100%">
              </div>
            </div>
          </div>
          <br>
          <b>MAPA 3</b>

          <div class="row">
            <div class="col-md-6">
              <b>LATITUD</b>
              <input type="text" required name="marcador3_lt" class="form-control" value="" id="latitud_lt3" readonly>
            </div>
            <div class="col-md-6">
              <b>LONGITUD</b>
              <input type="text" required name="marcador3_ln" class="form-control" value="" id="longitud_lt3" readonly>
            </div>
          </div>
          <div class="row">
          <br>
            <div class="col-md-12">
              <div class="" id="mapa3" style="border: 1px solid black; height: 250px; width: 100%">
              </div>
            </div>
          </div>
          <br>
          <b>MAPA 4</b>

          <div class="row">
            <div class="col-md-6">
              <b>LATITUD</b>
              <input type="text" required name="marcador4_lt" class="form-control" value="" id="latitud_lt4" readonly>
            </div>
            <div class="col-md-6">
              <b>LONGITUD</b>
              <input type="text" required name="marcador4_ln" class="form-control" value="" id="longitud_lt4" readonly>
            </div>
          </div>
          <div class="row">
          <br>
            <div class="col-md-12">
              <div class="" id="mapa4" style="border: 1px solid black; height: 250px; width: 100%">
              </div>
            </div>
          </div>
          <br>
          <button type="submit" class="btn btn-primary">
            Guardar
          </button>
          <button type="reset" class="btn btn-danger">
            Cancelar
          </button>
        </form>
        <br>
      </div>
    </div>
    <div class="col-md-6">
      <h3 class="text-center"><b>GEOREFERENCIACION</b></h3>
      <?php if ($listadoLocal) : ?>
        <table class="table table-stripe table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center">ID</th>
              <th class="text-center">CEDULA</th>
              <th class="text-center">APELLIDOS</th>
              <th class="text-center">NOMBRES</th>
              <th class="text-center">ACCIONES</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($listadoLocal as $puntoTemporal) : ?>
              <tr>
                <td class="text-center"> <?php echo $puntoTemporal->id_lt; ?> </td>
                <td class="text-center"> <?php echo $puntoTemporal->cedula_lt; ?></td>
                <td class="text-center"> <?php echo $puntoTemporal->apellido_lt; ?></td>
                <td class="text-center"> <?php echo $puntoTemporal->nombre_lt; ?></td>
                <td>
                  <a href="<?php echo site_url('puntos/borrar'); ?>/<?php echo $puntoTemporal->id_lt; ?>" class="btn btn-success">Ver</a>
                  <a href="<?php echo site_url('puntos/borrar'); ?>/<?php echo $puntoTemporal->id_lt; ?>" class="btn btn-danger">Eliminar</a>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      <?php else : ?>
        <h3><b>No existen puntos</b></h3>
      <?php endif; ?>

      <div class="row">
        <div class="col-md-12">
          <div class="" id="mapa5" style="border: 1px solid black; height: 250px; width: 100%">
          </div>
        </div>
      </div>
    </div>
  </div>


  <script type="text/javascript">
    //! script para la segmentacion de los mapas y ubicacion de los puntos
    function initMap() {
      latitud_longitud1 = new google.maps.LatLng(-0.9374805, -78.6161);
      var mapa1 = new google.maps.Map(
        document.getElementById('mapa1'), {
          center: latitud_longitud1,
          zoom: 13,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
      );
      //! Marcador del primer mapa
      var marcador1 = new google.maps.Marker({
        position: latitud_longitud1,
        map: mapa1,
        title: "seleccione la direccion",
        draggable: true
      });
      google.maps.event.addListener(
        marcador1,
        'dragend',
        function(event) {
          var latitud = this.getPosition().lat();
          var longitud = this.getPosition().lng();
          document.getElementById("latitud_lt1").value = JSON.stringify(latitud);
          document.getElementById("longitud_lt1").value = JSON.stringify(longitud);
        }
      );

      //! Segundo mapa de la lista

      latitud_longitud2 = new google.maps.LatLng(-0.9405855864492367, -78.61369689171718);
      var mapa2 = new google.maps.Map(
        document.getElementById('mapa2'), {
          center: latitud_longitud2,
          zoom: 13,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
      );
      //! Marcador del segundo mapa
      var marcador2 = new google.maps.Marker({
        position: latitud_longitud2,
        map: mapa2,
        title: "seleccione la direccion",
        draggable: true
      });
      google.maps.event.addListener(marcador2, 'dragend',
        function(event) {
          var latitud = this.getPosition().lat();
          var longitud = this.getPosition().lng();
          document.getElementById("latitud_lt2").value = latitud;
          document.getElementById("longitud_lt2").value = longitud;
        }
      );

      //! Tercer mapa de la lista
      latitud_longitud3 = new google.maps.LatLng(-0.9405855864492367, -78.61369689171718);
      var mapa3 = new google.maps.Map(
        document.getElementById('mapa3'), {
          center: latitud_longitud3,
          zoom: 13,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
      );
      //! Marcadoor del Tercer mapa
      var marcador3 = new google.maps.Marker({
        position: latitud_longitud3,
        map: mapa3,
        title: "seleccione la direccion",
        draggable: true
      });
      google.maps.event.addListener(
        marcador3,
        'dragend',
        function(event) {
          var latitud = this.getPosition().lat();
          var longitud = this.getPosition().lng();
          document.getElementById("latitud_lt3").value = latitud;
          document.getElementById("longitud_lt3").value = longitud;
        }
      );

      //! cuarto mapa
      latitud_longitud4 = new google.maps.LatLng(-0.9405855864492367, -78.61369689171718);
      var mapa4 = new google.maps.Map(
        document.getElementById('mapa4'), {
          center: latitud_longitud4,
          zoom: 13,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
      );
      //! Marcador del cuarto mapa
      var marcador4 = new google.maps.Marker({
        position: latitud_longitud4,
        map: mapa4,
        title: "seleccione la direccion",
        draggable: true
      });
      google.maps.event.addListener(
        marcador4,
        'dragend',
        function(event) {
          var latitud = this.getPosition().lat();
          var longitud = this.getPosition().lng();
          document.getElementById("latitud_lt4").value = latitud;
          document.getElementById("longitud_lt4").value = longitud;
        }
      );

      //TODO Mapa del listado de las personas
      var mapa5 = new google.maps.Map(
        document.getElementById('mapa5'), {
          center: new google.maps.LatLng(-0.9405855864492367, -78.61369689171718),
          zoom: 13,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
      );
    }
  </script>


</body>

</html>