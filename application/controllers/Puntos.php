<?php

class Puntos extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('punto');
    }


    public function index()
    {
        $data["listadoLocal"] = $this->punto->obtenerTodos();
        //print_r($listadoClientes->result());
        $this->load->view("punto/index", $data);
    }

    public function guardar()
    {
        $puntos1 = $this->input->post('marcador1_lt');
        $puntos2 = $this->input->post('marcador1_ln');
        $puntos3 = $this->input->post('marcador2_lt');
        $puntos4 = $this->input->post('marcador2_ln');
        $puntos5 = $this->input->post('marcador3_lt');
        $puntos6 = $this->input->post('marcador3_ln');
        $puntos7 = $this->input->post('marcador4_lt');
        $puntos8 = $this->input->post('marcador4_ln');
        $datos = array(
            "cedula_lt" => $this->input->post("cedula_lt"),
            "apellido_lt" => $this->input->post("apellido_lt"),
            "nombre_lt" => $this->input->post("nombre_lt"),
            "marcador1_lt" => '{"lat":' . $puntos1 . ',"lng":' . $puntos2 . '}',
            "marcador2_lt" => '{"lat":' . $puntos3 . ',"lng":' . $puntos4 . '}',
            "marcador3_lt" => '{"lat":' . $puntos5 . ',"lng":' . $puntos6 . '}',
            "marcador4_lt" => '{"lat":' . $puntos7 . ',"lng":' . $puntos8 . '}',
        );
        if ($this->punto->insertar($datos)) {
            redirect("puntos/index");
        } else {
            echo "error al insertar";
        }
    }
    //Funcion para eliminar ubicacion
	public function borrar($id_lt){
		if ($this->punto->eliminarPorId($id_lt)) {
			redirect('puntos/index');
		} else {
			echo "Error al eliminar";
		}
		
}
}